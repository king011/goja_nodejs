package events

import (
	"container/list"
	"errors"

	"github.com/dop251/goja"
)

// ErrListenerMustBeCallable .
var ErrListenerMustBeCallable = errors.New(`listener must be callable`)

// ErrMaxListenersExceededWarning .
var ErrMaxListenersExceededWarning = errors.New(`max listeners exceeded warning`)

type _Callable struct {
	Value    goja.Value
	Callable goja.Callable
	Once     bool
}
type _Listeners struct {
	item *list.List
}

func (l *_Listeners) AddListener(val goja.Value, callable goja.Callable,
	once, front bool,
	maxListeners int,
) (e error) {
	if maxListeners == 0 {
		e = ErrMaxListenersExceededWarning
		return
	}
	if l.item == nil {
		l.item = list.New()
	}

	if l.item.Len() >= maxListeners {
		e = ErrMaxListenersExceededWarning
		return
	}
	if front {
		l.item.PushFront(_Callable{
			Value:    val,
			Callable: callable,
			Once:     once,
		})
	} else {
		l.item.PushBack(_Callable{
			Value:    val,
			Callable: callable,
			Once:     once,
		})
	}
	return
}
func (l *_Listeners) RemoveListener(value goja.Value) {
	if l.item == nil {
		return
	}

	for element := l.item.Front(); element != nil; element = element.Next() {
		callable := element.Value.(_Callable)
		if callable.Value.Equals(value) {
			l.item.Remove(element)
			break
		}
	}
}
func (l *_Listeners) Emit(call goja.FunctionCall) (ok bool, e error) {
	if l.item == nil || l.item.Len() == 0 {
		return
	}
	ok = true
	var next *list.Element
	element := l.item.Front()
	for element != nil {
		callable := element.Value.(_Callable)
		_, e = callable.Callable(call.This, call.Arguments...)
		if e != nil {
			break
		}
		next = element.Next()
		if callable.Once {
			l.item.Remove(element)
		}
		element = next
	}
	return
}

type eventEmitter struct {
	vm           *goja.Runtime
	loop         *Loop
	listeners    map[goja.Value]*_Listeners
	ok           chan bool
	maxListeners int
}

func newEventEmitter(vm *goja.Runtime, loop *Loop) *eventEmitter {
	return &eventEmitter{
		vm:           vm,
		loop:         loop,
		listeners:    make(map[goja.Value]*_Listeners),
		ok:           make(chan bool, 1),
		maxListeners: 10,
	}
}

func (emitter *eventEmitter) nativeEmit(call goja.FunctionCall) goja.Value {
	key := call.Argument(0)
	if listeners, ok := emitter.listeners[key]; ok {
		call.Arguments = call.Arguments[1:]
		ok, e := listeners.Emit(call)
		if e != nil {
			panic(emitter.vm.ToValue(e))
		}
		return emitter.vm.ToValue(ok)
	}
	return emitter.vm.ToValue(false)
}
func (emitter *eventEmitter) getOrCreateListeners(key goja.Value) *_Listeners {
	listeners, ok := emitter.listeners[key]
	if ok {
		return listeners
	}
	listeners = &_Listeners{}
	emitter.listeners[key] = listeners
	return listeners
}
func (emitter *eventEmitter) nativeAddListener(call goja.FunctionCall) goja.Value {
	val := call.Argument(1)
	callable, ok := goja.AssertFunction(val)
	if !ok {
		panic(emitter.vm.ToValue(ErrListenerMustBeCallable))
	}
	key := call.Argument(0)
	listeners := emitter.getOrCreateListeners(key)
	e := listeners.AddListener(val, callable,
		false, false,
		emitter.maxListeners,
	)
	if e != nil {
		panic(emitter.vm.ToValue(e))
	}
	return call.This
}
func (emitter *eventEmitter) nativeOnce(call goja.FunctionCall) goja.Value {
	val := call.Argument(1)
	callable, ok := goja.AssertFunction(val)
	if !ok {
		panic(emitter.vm.ToValue(ErrListenerMustBeCallable))
	}
	key := call.Argument(0)
	listeners := emitter.getOrCreateListeners(key)
	e := listeners.AddListener(val, callable,
		true, false,
		emitter.maxListeners,
	)
	if e != nil {
		panic(emitter.vm.ToValue(e))
	}
	return call.This
}
func (emitter *eventEmitter) nativePrependListener(call goja.FunctionCall) goja.Value {
	val := call.Argument(1)
	callable, ok := goja.AssertFunction(val)
	if !ok {
		panic(emitter.vm.ToValue(ErrListenerMustBeCallable))
	}
	key := call.Argument(0)
	listeners := emitter.getOrCreateListeners(key)
	e := listeners.AddListener(val, callable,
		false, true,
		emitter.maxListeners,
	)
	if e != nil {
		panic(emitter.vm.ToValue(e))
	}
	return call.This
}
func (emitter *eventEmitter) nativePrependOnceListener(call goja.FunctionCall) goja.Value {
	val := call.Argument(1)
	callable, ok := goja.AssertFunction(val)
	if !ok {
		panic(emitter.vm.ToValue(ErrListenerMustBeCallable))
	}
	key := call.Argument(0)
	listeners := emitter.getOrCreateListeners(key)
	e := listeners.AddListener(val, callable,
		true, true,
		emitter.maxListeners,
	)
	if e != nil {
		panic(emitter.vm.ToValue(e))
	}
	return call.This
}
func (emitter *eventEmitter) nativeRemoveListener(call goja.FunctionCall) goja.Value {
	val := call.Argument(1)
	_, ok := goja.AssertFunction(val)
	if !ok {
		panic(emitter.vm.ToValue(ErrListenerMustBeCallable))
	}

	key := call.Argument(0)
	if listeners, ok := emitter.listeners[key]; ok {
		listeners.RemoveListener(val)
	}
	return call.This
}
func (emitter *eventEmitter) nativeRemoveAllListeners(call goja.FunctionCall) goja.Value {
	key := call.Argument(0)
	if _, ok := emitter.listeners[key]; ok {
		delete(emitter.listeners, key)
	}
	return call.This
}
func (emitter *eventEmitter) nativeGetMaxListeners(call goja.FunctionCall) goja.Value {
	return emitter.vm.ToValue(emitter.maxListeners)
}
func (emitter *eventEmitter) nativeSetMaxListeners(call goja.FunctionCall) goja.Value {
	if len(call.Arguments) < 1 {
		panic(emitter.vm.ToValue(errors.New(`It must be a non-negative number`)))
	}
	maxListeners := call.Arguments[0].ToInteger()
	if maxListeners < 0 {
		panic(emitter.vm.ToValue(errors.New(`It must be a non-negative number`)))
	}
	emitter.maxListeners = int(maxListeners)
	return call.This
}
func (emitter *eventEmitter) nativeListeners(call goja.FunctionCall) goja.Value {
	key := call.Argument(0)
	var arrs []goja.Value
	if listeners, ok := emitter.listeners[key]; ok {
		list := listeners.item
		if list != nil {
			count := list.Len()
			arrs = make([]goja.Value, 0, count)
			for element := list.Front(); element != nil; element = element.Next() {
				callable := element.Value.(_Callable)
				arrs = append(arrs, callable.Value)
			}
		}
	}
	if arrs == nil {
		arrs = make([]goja.Value, 0)
	}
	return emitter.vm.ToValue(arrs)
}
func (emitter *eventEmitter) nativeListenerCount(call goja.FunctionCall) goja.Value {
	key := call.Argument(0)
	var listenerCount int
	if listeners, ok := emitter.listeners[key]; ok {
		list := listeners.item
		if list != nil {
			listenerCount = listeners.item.Len()
		}
	}
	return emitter.vm.ToValue(listenerCount)
}
func (emitter *eventEmitter) nativeEventNames(call goja.FunctionCall) goja.Value {
	count := len(emitter.listeners)
	names := make([]goja.Value, 0, count)
	for name := range emitter.listeners {
		names = append(names, name)
	}
	return emitter.vm.ToValue(names)
}

type wrapperEventEmitter struct {
	vm   *goja.Runtime
	loop *Loop
}

func (wrapper *wrapperEventEmitter) Constructor(call goja.ConstructorCall) *goja.Object {
	obj := call.This
	emitter := newEventEmitter(wrapper.vm, wrapper.loop)
	obj.Set(`emit`, emitter.nativeEmit)
	obj.Set(`addListener`, emitter.nativeAddListener)
	obj.Set(`on`, emitter.nativeAddListener)
	obj.Set(`once`, emitter.nativeOnce)
	obj.Set(`removeListener`, emitter.nativeRemoveListener)
	obj.Set(`off`, emitter.nativeRemoveListener)
	obj.Set(`removeAllListeners`, emitter.nativeRemoveAllListeners)
	obj.Set(`getMaxListeners`, emitter.nativeGetMaxListeners)
	obj.Set(`setMaxListeners`, emitter.nativeSetMaxListeners)
	obj.Set(`listeners`, emitter.nativeListeners)
	obj.Set(`rawListeners`, emitter.nativeListeners)
	obj.Set(`listenerCount`, emitter.nativeListenerCount)
	obj.Set(`prependListener`, emitter.nativePrependListener)
	obj.Set(`prependOnceListener`, emitter.nativePrependOnceListener)
	obj.Set(`eventNames`, emitter.nativeEventNames)
	return nil
}

// EventEmitterHelper .
type EventEmitterHelper struct {
	vm   *goja.Runtime
	self *goja.Object
	emit goja.Callable
}

// NewEventEmitterHelper .
func NewEventEmitterHelper(vm *goja.Runtime, self *goja.Object) *EventEmitterHelper {
	return &EventEmitterHelper{
		vm:   vm,
		self: self,
	}
}

// Emit .
func (emitter *EventEmitterHelper) Emit(args ...goja.Value) (ok bool) {
	if emitter.emit == nil {
		self := emitter.self
		v := self.Get(`emit`)
		if v == nil {
			return
		}
		callable, yes := goja.AssertFunction(v)
		if !yes {
			return
		}
		emitter.emit = callable
	}
	vm := emitter.vm
	v, e := emitter.emit(vm.ToValue(emitter.self), args...)
	if e != nil {
		panic(vm.NewGoError(e))
	}
	if v == nil {
		return
	}
	ok = v.ToBoolean()
	return
}
