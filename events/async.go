package events

// Async .
type Async interface {
	// 向異步操作 發送一個數據
	Next(handler AsyncHandler) (ok bool)
	// 撤銷 異步操作
	Cancel() bool
}

// AsyncHandler .
type AsyncHandler func(canceled bool) (completed bool, e error)

type _Event struct {
	Async   *_Async
	Handler AsyncHandler
}
type _Async struct {
	Loop *Loop
}

func (a *_Async) Next(handler AsyncHandler) (ok bool) {
	select {
	case <-a.Loop.ch: // clsoed
	case a.Loop.events <- &_Event{
		Async:   a,
		Handler: handler,
	}:
		ok = true
	}
	return
}
func (a *_Async) Cancel() (ok bool) {
	_, ok = a.Loop.keys[a]
	if ok {
		delete(a.Loop.keys, a)
	}
	return
}
