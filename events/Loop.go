package events

import (
	"errors"
	"sync/atomic"
)

// ErrClosed .
var ErrClosed = errors.New(`loop closed`)

// Loop 事件循環
type Loop struct {
	events chan *_Event
	closed int32
	ch     chan struct{}
	keys   map[*_Async]bool
}

func newLoop() *Loop {
	return &Loop{
		events: make(chan *_Event),
		ch:     make(chan struct{}),
		keys:   make(map[*_Async]bool),
	}
}

// Done returns a channel that's closed when work done on behalf of this
func (l *Loop) Done() <-chan struct{} {
	return l.ch
}

// Close .
func (l *Loop) Close() (e error) {
	if atomic.CompareAndSwapInt32(&l.closed, 0, 1) {
		close(l.ch)
	} else {
		e = ErrClosed
	}
	return
}

// Run 執行事件循環
func (l *Loop) Run() (e error) {
	var (
		work      = true
		completed = false
	)
	for work && len(l.keys) != 0 {
		select {
		case <-l.ch: //closed
			work = false
		case event := <-l.events: //callback
			found := l.keys[event.Async]
			completed, e = event.Handler(!found)
			if found {
				if completed {
					delete(l.keys, event.Async)
				}
				if e != nil {
					work = false
				}
			}
		}
	}
	return
}

// Async 創建一個 異步流程
func (l *Loop) Async() Async {
	async := &_Async{
		Loop: l,
	}
	l.keys[async] = true
	return async
}
