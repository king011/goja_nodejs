package events

import (
	"time"

	"github.com/dop251/goja"
)

type _Timeout struct {
	loop  *Loop
	async Async

	call     goja.FunctionCall
	callable goja.Callable

	timer *time.Timer
}

func newTimeout(
	loop *Loop,
	callable goja.Callable, call goja.FunctionCall,
	duration time.Duration,
) *_Timeout {
	timeout := &_Timeout{
		loop:     loop,
		async:    loop.Async(),
		callable: callable,
		call:     call,

		timer: time.NewTimer(duration),
	}
	go timeout.serve()
	return timeout
}
func (t *_Timeout) serve() {
	select {
	case <-t.timer.C:
		t.async.Next(t.asyncHandler)
	case <-t.loop.ch: // loop closed
		if !t.timer.Stop() {
			<-t.timer.C
		}
	}
}
func (t *_Timeout) asyncHandler(canceled bool) (completed bool, e error) {
	if canceled {
		return
	}
	completed = true
	if t.callable != nil {
		_, e = t.callable(t.call.This, t.call.Arguments...)
		if e != nil {
			return
		}
	}
	return
}

func (t *_Timeout) Stop() {
	t.async.Cancel()
	t.timer.Stop()
}

type _Interval struct {
	loop  *Loop
	async Async

	call     goja.FunctionCall
	callable goja.Callable

	duration time.Duration
	timer    *time.Timer
	closed   bool
	ch       chan struct{}
}

func newInterval(
	loop *Loop,
	callable goja.Callable, call goja.FunctionCall,
	duration time.Duration,
) *_Interval {
	interval := &_Interval{
		loop:     loop,
		async:    loop.Async(),
		callable: callable,
		call:     call,

		duration: duration,
		timer:    time.NewTimer(duration),
		ch:       make(chan struct{}),
	}
	go interval.serve()
	return interval
}
func (t *_Interval) serve() {
	work := true
	for work {
		select {
		case <-t.ch:
			work = false
		case <-t.timer.C:
			t.async.Next(t.asyncHandler)
		case <-t.loop.ch: // loop closed
			if !t.timer.Stop() {
				<-t.timer.C
			}
			work = false
		}
	}
}
func (t *_Interval) asyncHandler(canceled bool) (completed bool, e error) {
	if canceled {
		return
	}
	completed = t.closed
	if t.callable != nil {
		_, e = t.callable(t.call.This, t.call.Arguments...)
		if e != nil {
			return
		}
	}
	if !completed {
		t.timer.Reset(t.duration)
		go t.serve()
	}
	return
}
func (t *_Interval) Stop() {
	if t.closed {
		return
	}
	t.closed = true

	t.async.Cancel()
	t.timer.Stop()
}

type nativeTimer struct {
	vm   *goja.Runtime
	loop *Loop
}

func newTimer(vm *goja.Runtime, loop *Loop) *nativeTimer {
	return &nativeTimer{
		vm:   vm,
		loop: loop,
	}
}
func (t *nativeTimer) init() {
	vm := t.vm
	vm.Set(`setTimeout`, t.nativeSetTimeout)
	vm.Set(`clearTimeout`, t.nativeClearTimeout)
	vm.Set(`setInterval`, t.nativeSetInterval)
	vm.Set(`clearInterval`, t.nativeClearInterval)
}
func (t *nativeTimer) nativeSetTimeout(call goja.FunctionCall) goja.Value {
	callable, ok := goja.AssertFunction(call.Argument(0))
	if !ok {
		panic(t.vm.ToValue(ErrListenerMustBeCallable))
	}
	duration := time.Duration(call.Argument(1).ToInteger()) * time.Millisecond
	if len(call.Arguments) >= 2 {
		call.Arguments = call.Arguments[2:]
	} else {
		call.Arguments = call.Arguments[1:]
	}
	timeout := newTimeout(t.loop, callable, call, duration)
	return t.vm.ToValue(timeout)
}
func (t *nativeTimer) nativeClearTimeout(call goja.FunctionCall) goja.Value {
	if len(call.Arguments) > 0 {
		var timeout *_Timeout
		e := t.vm.ExportTo(call.Arguments[0], &timeout)
		if e == nil {
			timeout.Stop()
		}
	}
	return nil
}
func (t *nativeTimer) nativeSetInterval(call goja.FunctionCall) goja.Value {
	callable, ok := goja.AssertFunction(call.Argument(0))
	if !ok {
		panic(t.vm.ToValue(ErrListenerMustBeCallable))
	}
	duration := time.Duration(call.Argument(1).ToInteger()) * time.Millisecond
	if len(call.Arguments) >= 2 {
		call.Arguments = call.Arguments[2:]
	} else {
		call.Arguments = call.Arguments[1:]
	}
	interval := newInterval(t.loop, callable, call, duration)
	return t.vm.ToValue(interval)
}
func (t *nativeTimer) nativeClearInterval(call goja.FunctionCall) goja.Value {
	if len(call.Arguments) > 0 {
		var interval *_Interval
		e := t.vm.ExportTo(call.Arguments[0], &interval)
		if e == nil {
			interval.Stop()
		}
	}
	return nil
}
