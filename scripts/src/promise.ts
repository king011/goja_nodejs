new Promise<undefined>((resolve) => {
    setTimeout(resolve, 100)
}).finally().then(() => {
    throw new Error("123");
}).finally().then((e) => {
    console.log('then')
}).catch((e) => {
    console.log('catch')
})
function sleep(timeout: number): Promise<undefined> {
    return new Promise<undefined>((resolve) => {
        setTimeout(resolve, timeout)
    })
}

(function () {
    console.log('1')
    sleep(1000).then(async () => {
        console.log('3')
    })
    console.log('2')
    const p = sleep(1000)
    p.catch((e) => {
        console.log(e)
    })
    p.finally(() => {
        console.log('finally 1')
        throw 123;
    }).catch((e) => {
        console.log(e)
    })

    p.finally(() => {
        console.log('finally 2')
        throw 456;
    }).catch((e) => {
        console.log(e)
    })
})()