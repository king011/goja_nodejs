import { module } from 'qunit';

import { TestWritable } from './writable'
import { TestReadable } from './readable'

module(`stream`, () => {
    TestWritable()
    TestReadable()
})