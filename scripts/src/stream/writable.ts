import { test } from 'qunit';
import { EventEmitter } from 'events'
import { Writable } from 'stream'
export function TestWritable() {
    test(`Writable`, (assert) => {
        const w = new Writable({
            highWaterMark: 123,
        })
        assert.true(w.writable, `writable`)
        assert.false(w.writableEnded, `writableEnded`)
        assert.false(w.writableFinished, `writableFinished`)

        assert.equal(123, w.writableHighWaterMark, `writableHighWaterMark`)
        assert.equal(0, w.writableLength, `writableLength`)
        assert.false(w.writableObjectMode, `writableObjectMode`)
        assert.equal(0, w.writableCorked, `writableCorked`)
        assert.false(w.destroyed, `destroyed`)
        assert.ok(w instanceof EventEmitter, `Writable not instanceof EventEmitter`)
        w.on(`error`, (e) => {
            console.log(e.message)
        })
        w.destroy()
        w.write("123")
    })
    test(`Writable.write`, (assert) => {
        const source = new Array<string>()
        class BufferString extends Writable {
            private data_ = new Array<string>()
            constructor() {
                super({
                    objectMode: true,
                    decodeStrings: false,
                    // autoDestroy: true,
                    write(this: BufferString, chunk: any, encoding: BufferEncoding, callback: (error?: Error | null) => void): void {
                        console.log(typeof chunk, chunk instanceof Uint8Array)
                        this.data_.push(chunk)
                        callback()
                    }
                })
            }
            println() {
                console.log(this.data_.length)
                console.log(this.data_)
            }
        }
        const w = new BufferString()
        w.on(`error`, (e) => {
            console.log("error", e.message)
        }).on(`drain`, () => {
            console.log(`drain`)
        }).on(`finish`, () => {
            console.log(`finish`)
        }).on(`close`, () => {
            console.log(`close`)
        })
        assert.true(true)
        w.write("123")
        w.write(Uint8Array.from([1, 2, 3]))
        w.write(true)
        return
        // assert.true(w.write("123"), "utf8")
        try {
            console.log('--------------', w.write(undefined, (e) => {
                console.log('dw', e.message)
            }))
        } catch (e) { console.log('catch') }
        const ww = assert.async()
        setTimeout(() => {
            ww()
        }, 1000);
        return
        assert.true(w.write("123"), "utf8")
        assert.true(w.write(
            Uint8Array.from([1, 2, 3]),
            (e) => {
                console.log('we')
            },
        ))
        assert.true(w.write("456"))

        // w.end()
        w.println()
    })
}