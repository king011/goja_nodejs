import { EventEmitter } from "events"
console.log(__dirname)
console.log(__filename)

const emitter = new EventEmitter()

emitter.on('test', (a, b, c) => {
    console.log('test', a, b, c)
})
const addListener = (a: any, b: any, c: any) => {
    console.log('addListener', a, b, c)
}
const on = function (a: any, b: any, c: any) {
    console.log('on', a, b, c)
}
const once = function (a: any, b: any, c: any) {
    console.log('once', a, b, c)
    emitter.emit('test', 100, 101, 102)
}

emitter.addListener("ok", addListener)
emitter.once("ok", once)
emitter.on("ok", on)
emitter.addListener("ok", addListener)

emitter.emit("ok", 1, 2, 3)
emitter.emit("ok", 4, 5, 6)

emitter.removeListener("ok", addListener)
emitter.emit("ok", 7, 8, 9)

emitter.off("ok", addListener)
emitter.emit("ok", 10, 11, 12)

emitter.removeAllListeners("ok")
emitter.emit("ok", 13, 14, 15)

console.log('prepend')
emitter.on("ok", on)
emitter.prependListener("ok", addListener)
emitter.prependOnceListener("ok", once)
emitter.emit("ok", "a", "b", "c")

console.log(emitter.listeners("ok").length)
console.log(emitter.rawListeners("ok").length)
console.log(emitter.listenerCount("ok"))
console.log(emitter.eventNames())

const t0 = setTimeout(() => {
    console.log('timer t0')
}, 1000)

setTimeout((a: any, b: any, c: any) => {
    clearTimeout(t0)
    console.log('timer', a, b, c)

    var i = 0
    var t = setInterval(() => {
        i++
        console.log(i)
        if (i == 10) {
            clearInterval(t)
        }
    }, 100)
}, 500, 'a', 'b', 'c')
console.log('setTimeout', setTimeout)