package helper

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/dop251/goja_nodejs/require"
)

// SourceLoader .
func SourceLoader(filename string) ([]byte, error) {
	source, e := LoadSource(filename)
	if e != nil {
		return nil, e
	}
	return []byte(source), nil
}

// LoadSource .
func LoadSource(filename string) (source string, e error) {
	var b []byte
	f, e := os.Open(filename)
	if e != nil {
		if os.IsNotExist(e) {
			e = require.ModuleFileDoesNotExistError
		}
		return
	}
	stat, e := f.Stat()
	if e != nil {
		f.Close()
		return
	}
	if stat.IsDir() {
		f.Close()
		e = require.ModuleFileDoesNotExistError
		return
	}
	b, e = ioutil.ReadAll(f)
	f.Close()
	if e != nil {
		return
	}
	ext := strings.ToLower(filepath.Ext(filename))
	if ext == ".js" {
		source = fmt.Sprintf(`var module = module || { exports: {} };(function(__dirname,__filename,module,exports){%s
})(%q,%q,module,module.exports)`,
			b,
			filepath.Dir(filename), filename,
		)
	} else {
		source = string(b)
	}
	return
}
