package util

import (
	"github.com/dop251/goja"
	"github.com/dop251/goja_nodejs/require"
)

// NativeExtends .
type NativeExtends struct {
	vm        *goja.Runtime
	jsExtends func(d, b *goja.Object)
}

func (t *NativeExtends) init(vm *goja.Runtime, obj *goja.Object) {
	t.vm = vm
	v, e := vm.RunString(`
function __extends(d, b) {
	Object.setPrototypeOf(d.prototype, b.prototype);
}
__extends;
`)
	if e != nil {
		panic(vm.ToValue(e))
	}
	e = vm.ExportTo(v, &t.jsExtends)
	if e != nil {
		panic(vm.ToValue(e))
	}
	obj.Set(`__nativeExtends`, t)
}

// GetExtends .
func GetExtends(vm *goja.Runtime) (extends *NativeExtends) {
	v := require.Require(vm, ModuleID)
	obj := v.(*goja.Object)
	v = obj.Get(`__nativeExtends`)
	e := vm.ExportTo(v, &extends)
	if e != nil {
		panic(vm.ToValue(e))
	}
	return
}

// Extends .
func (t *NativeExtends) Extends(superObj *goja.Object, factory func(func(goja.FunctionCall) goja.Value) func(call goja.ConstructorCall) *goja.Object) *goja.Object {
	vm := t.vm
	var super func(goja.FunctionCall) goja.Value
	e := vm.ExportTo(superObj, &super)
	if e != nil {
		panic(vm.ToValue(e))
	}
	ctor := vm.ToValue(factory(super)).(*goja.Object)
	t.jsExtends(ctor, superObj)
	return ctor
}
