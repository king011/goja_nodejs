package util

import "github.com/dop251/goja"

// IsNullOrUndefined v is null | undefined
func IsNullOrUndefined(v goja.Value) bool {
	return v == nil || goja.IsNull(v) || goja.IsUndefined(v)
}
