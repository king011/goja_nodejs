package util

import (
	"github.com/dop251/goja"
)

const (
	// ModuleID .
	ModuleID = `util`
)

// Require 加載模塊
func Require(vm *goja.Runtime, module *goja.Object) {
	obj := module.Get(`exports`).(*goja.Object)
	var extends NativeExtends
	extends.init(vm, obj)
}
