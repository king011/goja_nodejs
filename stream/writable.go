package stream

import (
	"errors"

	"github.com/dop251/goja"
	"gitlab.com/king011/goja_nodejs/events"
	"gitlab.com/king011/goja_nodejs/util"
)

const (
	defaultEncoding = `utf8`
)

var (
	// ErrUnknowEventEmitter .
	ErrUnknowEventEmitter = errors.New(`unknown symbol EventEmitter, please require events module`)
	// ErrWritableWriteUnimplemented .
	ErrWritableWriteUnimplemented = errors.New(`The Writable.write() method is not implemented`)
	// ErrWritableWriteAfterEnd .
	ErrWritableWriteAfterEnd = errors.New(`Writable.write after end`)
	// ErrWritableWriteAfterDestroyed .
	ErrWritableWriteAfterDestroyed = errors.New(`Writable.write after destroyed`)
	// ErrWritableDestroyed .
	ErrWritableDestroyed = errors.New(`Writable destroyed`)
)

type wrapper struct {
	vm      *goja.Runtime
	extends *util.NativeExtends

	eventEmitter *goja.Object
}

func (w *wrapper) Writable() *goja.Object {
	superObj, e := w.getEventEmitter()
	if e != nil {
		panic(w.vm.ToValue(w.vm.NewGoError(e)))
	}
	return w.extends.Extends(superObj, func(super func(goja.FunctionCall) goja.Value) func(call goja.ConstructorCall) *goja.Object {
		return func(call goja.ConstructorCall) *goja.Object {
			self := call.This
			// call super()
			v := super(goja.FunctionCall{
				This:      self,
				Arguments: call.Arguments,
			})
			if o, ok := v.(*goja.Object); ok {
				self = o
			}
			// extends
			newWritable(w.vm, self).extends(call.Argument(0))
			return self
		}
	})
}
func (w *wrapper) getEventEmitter() (eventEmitter *goja.Object, e error) {
	if w.eventEmitter != nil {
		eventEmitter = w.eventEmitter
		return
	}
	v, e := w.vm.RunString(`
var events = require("events");
events?events.EventEmitter:undefined;
`)
	if e != nil {
		return
	}
	var ok bool
	if eventEmitter, ok = v.(*goja.Object); ok {
		w.eventEmitter = eventEmitter
	} else {
		e = ErrUnknowEventEmitter
	}
	return
}

type nativeWritable struct {
	vm      *goja.Runtime
	self    *goja.Object
	emitter *events.EventEmitterHelper

	decodeStrings   bool
	defaultEncoding string
	emitClose       bool
	autoDestroy     bool

	write   goja.Callable
	writev  goja.Callable
	destroy goja.Callable
	final   goja.Callable

	writable              bool
	writableEnded         bool
	writableFinished      bool
	writableHighWaterMark int64
	writableLength        int64
	writableObjectMode    bool
	writableCorked        int64
	destroyed             bool

	drain         bool
	writing       bool
	finish        bool
	writeCallback goja.Value
	queue         *writeQueue
	writeCB       goja.Callable
}

func newWritable(vm *goja.Runtime, self *goja.Object) *nativeWritable {
	return &nativeWritable{
		vm:      vm,
		self:    self,
		emitter: events.NewEventEmitterHelper(vm, self),

		decodeStrings:   true,
		defaultEncoding: defaultEncoding,
		emitClose:       true,

		writable:              true,
		writableHighWaterMark: 16384,
	}
}
func (w *nativeWritable) extends(option goja.Value) {

	if opts, ok := option.(*goja.Object); ok {
		w.init(opts)
	}
	w.queue = newWriteQueue(w.writableHighWaterMark)
	w.writeCallback = w.vm.ToValue(w.nativeWriteCallback)
	w.extendsPropertys()
	w.extendsMethods()
}
func (w *nativeWritable) extendsPropertys() {
	self := w.self
	vm := w.vm
	self.DefineAccessorProperty(`writable`,
		vm.ToValue(w.nativeGetWritable),
		nil,
		goja.FLAG_TRUE, goja.FLAG_FALSE,
	)
	self.DefineAccessorProperty(`writableEnded`,
		vm.ToValue(w.nativeGetWritableEnded),
		nil,
		goja.FLAG_TRUE, goja.FLAG_FALSE,
	)
	self.DefineAccessorProperty(`writableFinished`,
		vm.ToValue(w.nativeGetWritableFinished),
		nil,
		goja.FLAG_TRUE, goja.FLAG_FALSE,
	)
	self.DefineAccessorProperty(`writableHighWaterMark`,
		vm.ToValue(w.nativeGetWritableHighWaterMark),
		nil,
		goja.FLAG_TRUE, goja.FLAG_FALSE,
	)
	self.DefineAccessorProperty(`writableLength`,
		vm.ToValue(w.nativeGetWritableLength),
		nil,
		goja.FLAG_TRUE, goja.FLAG_FALSE,
	)
	self.DefineAccessorProperty(`writableObjectMode`,
		vm.ToValue(w.nativeGetWritableObjectMode),
		nil,
		goja.FLAG_TRUE, goja.FLAG_FALSE,
	)
	self.DefineAccessorProperty(`writableCorked`,
		vm.ToValue(w.nativeGetWritableCorked),
		nil,
		goja.FLAG_TRUE, goja.FLAG_FALSE,
	)
	self.DefineAccessorProperty(`destroyed`,
		vm.ToValue(w.nativeGetDestroyed),
		nil,
		goja.FLAG_TRUE, goja.FLAG_FALSE,
	)
}
func (w *nativeWritable) extendsMethods() {
	self := w.self
	self.Set(`write`, w.nativeWrite)
	self.Set(`destroy`, w.nativeDestroy)
	self.Set(`end`, w.nativeEnd)
	// self.Set(`cork`, w.nativeCork)
	// self.Set(`uncork`, w.nativeUncork)
	// self.Set(`pipe`, w.nativePipe)
}
func (w *nativeWritable) init(option *goja.Object) {
	opt := option.Get(`highWaterMark`)
	if opt != nil {
		w.writableHighWaterMark = opt.ToInteger()
	}
	opt = option.Get(`decodeStrings`)
	if opt != nil {
		w.decodeStrings = opt.ToBoolean()
	}
	opt = option.Get(`defaultEncoding`)
	if opt != nil {
		w.defaultEncoding = opt.String()
	}
	opt = option.Get(`objectMode`)
	if opt != nil {
		w.writableObjectMode = opt.ToBoolean()
	}
	opt = option.Get(`emitClose`)
	if opt != nil {
		w.emitClose = opt.ToBoolean()
	}
	opt = option.Get(`autoDestroy`)
	if opt != nil {
		w.autoDestroy = opt.ToBoolean()
	}
	opt = option.Get(`write`)
	if opt != nil {
		w.write, _ = goja.AssertFunction(opt)
	}
	opt = option.Get(`writev`)
	if opt != nil {
		w.writev, _ = goja.AssertFunction(opt)
	}
	opt = option.Get(`destroy`)
	if opt != nil {
		w.destroy, _ = goja.AssertFunction(opt)
	}
	opt = option.Get(`final`)
	if opt != nil {
		w.final, _ = goja.AssertFunction(opt)
	}
}

func (w *nativeWritable) nativeGetWritable(call goja.FunctionCall) goja.Value {
	return w.vm.ToValue(w.writable)
}
func (w *nativeWritable) nativeGetWritableEnded(call goja.FunctionCall) goja.Value {
	return w.vm.ToValue(w.writableEnded)
}
func (w *nativeWritable) nativeGetWritableFinished(call goja.FunctionCall) goja.Value {
	return w.vm.ToValue(w.writableFinished)
}
func (w *nativeWritable) nativeGetWritableHighWaterMark(call goja.FunctionCall) goja.Value {
	return w.vm.ToValue(w.writableHighWaterMark)
}
func (w *nativeWritable) nativeGetWritableLength(call goja.FunctionCall) goja.Value {
	return w.vm.ToValue(w.writableLength)
}
func (w *nativeWritable) nativeGetWritableObjectMode(call goja.FunctionCall) goja.Value {
	return w.vm.ToValue(w.writableObjectMode)
}
func (w *nativeWritable) nativeGetWritableCorked(call goja.FunctionCall) goja.Value {
	return w.vm.ToValue(w.writableCorked)
}
func (w *nativeWritable) nativeGetDestroyed(call goja.FunctionCall) goja.Value {
	return w.vm.ToValue(w.destroyed)
}
func (w *nativeWritable) nativeWrite(call goja.FunctionCall) goja.Value {
	chunk, encoding, callable := w.getWriteOptions(call)
	if w.destroyed {
		if callable != nil {
			callable(goja.Undefined(), w.vm.NewGoError(ErrWritableWriteAfterDestroyed))
		}
		return w.vm.ToValue(false)
	}
	if w.write == nil && w.writev == nil {
		panic(w.vm.NewGoError(ErrWritableWriteUnimplemented))
	} else if w.writableEnded {
		if callable != nil {
			callable(goja.Undefined(), w.vm.NewGoError(ErrWritableWriteAfterDestroyed))
		}
		w.emitter.Emit(w.vm.ToValue(`error`), w.vm.NewGoError(ErrWritableWriteAfterEnd))
		return w.vm.ToValue(false)
	}
	if chunk == nil {
		return w.vm.ToValue(true)
	}
	e := w.checkWriteData(chunk, encoding)
	if e != nil {
		if callable != nil {
			callable(goja.Undefined(), w.vm.NewGoError(e))
		}
		w.emitter.Emit(w.vm.ToValue(`error`), w.vm.NewGoError(e))
		return w.vm.ToValue(false)
	}
	ok := w.postWrite(chunk, encoding, callable)
	return w.vm.ToValue(ok)
}
func (w *nativeWritable) getWriteOptions(call goja.FunctionCall) (chunk goja.Value, encoding string, callable goja.Callable) {
	count := len(call.Arguments)
	if count > 0 {
		chunk = call.Arguments[0]
	}
	encoding = w.defaultEncoding
	if count > 1 && !util.IsNullOrUndefined(call.Arguments[1]) {
		callable, _ = goja.AssertFunction(call.Arguments[1])
		if callable != nil {
			return
		}
		if str, ok := (call.Arguments[1].Export()).(string); ok {
			encoding = str
		}
	}
	if count > 2 && !util.IsNullOrUndefined(call.Arguments[2]) {
		callable, _ = goja.AssertFunction(call.Arguments[2])
	}
	return
}
func (w *nativeWritable) postWrite(chunk goja.Value, encoding string, callable goja.Callable) (ok bool) {
	if w.writing {
		// 加入緩存
		drain := w.queue.Push(chunk, encoding, callable)
		if drain {
			w.drain = drain
		}
		if !w.drain {
			ok = true
		}
	} else {
		w.writing = true
		w.writeCB = callable
		// 執行 寫入
		if w.write != nil {
			w.write(w.self, chunk, w.vm.ToValue(encoding), w.writeCallback)
		} else if w.writev != nil {
			w.writev(w.self, w.vm.ToValue(map[string]interface{}{
				`chunk`:    chunk,
				`encoding`: encoding,
			}), w.writeCallback)
		} else {
			panic(`Writable.write and Writable.writev is nil`)
		}
		ok = true
	}
	return
}
func (w *nativeWritable) nativeWriteCallback(call goja.FunctionCall) goja.Value {
	w.writing = false
	v := call.Argument(0)
	if util.IsNullOrUndefined(v) { // 成功
		if w.queue.IsEmpty() { // 全部寫完
			if w.drain {
				if !w.destroyed {
					w.emitter.Emit(w.vm.ToValue(`drain`))
				}
				w.drain = false
			}
			w.clearQueue()
			return nil
		}

		// 將緩存寫入
		w.postWrite(w.queue.Pop())
		return nil
	}
	// 失敗
	if w.autoDestroy {
		if w.writeCB != nil {
			w.writeCB(w.self, v)
		}
		// destory
		w.executeDestroy(nil)
		w.clearQueue()
		return nil
	}
	w.emitter.Emit(w.vm.ToValue(`error`), v)
	if w.writeCB != nil {
		w.writeCB(w.self, v)
	}
	if w.destroyed {
		w.clearQueue()
	} else {
		if !w.queue.IsEmpty() { // 全部寫完
			// 將緩存寫入
			w.postWrite(w.queue.Pop())
			return nil
		}
	}
	return nil
}
func (w *nativeWritable) clearQueue() {
	var callable goja.Callable
	for !w.queue.IsEmpty() {
		_, _, callable = w.queue.Pop()
		if callable != nil {
			callable(goja.Undefined(), w.vm.NewGoError(ErrWritableWriteAfterDestroyed))
		}
	}
	if w.finish {
		w.finish = false
		w.writableFinished = true
		w.emitter.Emit(w.vm.ToValue(`finish`))
		if !w.destroyed {
			w.executeDestroy(nil)
		}
	}
}
func (w *nativeWritable) executeDestroy(v goja.Value) {
	if w.destroyed {
		return
	}
	w.writable = false
	w.destroyed = true
	if w.destroy != nil {
		if v == nil {
			w.destroy(w.self)
		} else {
			w.destroy(w.self, v)
		}
	}
	w.emitter.Emit(w.vm.ToValue(`close`))
	if w.writing {
		return
	}
	w.clearQueue()
}
func (w *nativeWritable) nativeDestroy(call goja.FunctionCall) goja.Value {
	if w.destroyed {
		return nil
	}
	v := call.Argument(0)
	if util.IsNullOrUndefined(v) {
		w.executeDestroy(w.vm.NewGoError(ErrWritableDestroyed))
	} else {
		w.executeDestroy(v)
	}
	return nil
}
func (w *nativeWritable) nativeEnd(call goja.FunctionCall) goja.Value {
	// 驗證參數
	chunk, encoding, callable := w.getEndOptions(call)
	if w.finish {
		if chunk != nil {
			if callable != nil {
				callable(goja.Undefined(), w.vm.NewGoError(ErrWritableWriteAfterDestroyed))
			}
			w.emitter.Emit(w.vm.ToValue(`error`), w.vm.NewGoError(ErrWritableWriteAfterEnd))
		}
		return w.vm.ToValue(false)
	}
	w.finish = true
	w.writable = false
	if chunk == nil {
		if callable != nil {
			callable(goja.Undefined())
		}
		if !w.writing {
			w.clearQueue()
		}
	} else {
		if w.destroyed {
			if callable != nil {
				callable(goja.Undefined(), w.vm.NewGoError(ErrWritableWriteAfterDestroyed))
			}
			return w.vm.ToValue(false)
		}
		if w.write == nil && w.writev == nil {
			panic(w.vm.NewGoError(ErrWritableWriteUnimplemented))
		} else if w.writableEnded {
			if callable != nil {
				callable(goja.Undefined(), w.vm.NewGoError(ErrWritableWriteAfterDestroyed))
			}
			w.emitter.Emit(w.vm.ToValue(`error`), w.vm.NewGoError(ErrWritableWriteAfterEnd))
			return w.vm.ToValue(false)
		}
		e := w.checkWriteData(chunk, encoding)
		if e != nil {
			if callable != nil {
				callable(goja.Undefined(), w.vm.NewGoError(e))
			}
			w.emitter.Emit(w.vm.ToValue(`error`), w.vm.NewGoError(e))
			return w.vm.ToValue(false)
		}
		// 寫入數據
		w.postWrite(chunk, encoding, callable)
	}
	return w.vm.ToValue(false)
}
func (w *nativeWritable) getEndOptions(call goja.FunctionCall) (chunk goja.Value, encoding string, callable goja.Callable) {
	count := len(call.Arguments)
	if count > 0 && !util.IsNullOrUndefined(call.Arguments[0]) {
		callable, _ = goja.AssertFunction(call.Arguments[0])
		if callable != nil {
			return
		}
		chunk = call.Arguments[0]
	}
	encoding = w.defaultEncoding
	if count > 1 && !util.IsNullOrUndefined(call.Arguments[1]) {
		callable, _ = goja.AssertFunction(call.Arguments[1])
		if callable != nil {
			return
		}
		if str, ok := (call.Arguments[1].Export()).(string); ok {
			encoding = str
		}
	}
	if count > 2 && !util.IsNullOrUndefined(call.Arguments[2]) {
		callable, _ = goja.AssertFunction(call.Arguments[2])
	}
	return
}
func (w *nativeWritable) checkWriteData(v goja.Value, encoding string) error {
	// i := v.ExportType()
	// if _, ok := i.(string); ok {
	// 	fmt.Println(`string`)
	// } else {
	// 	if _, ok := i.(*goja.ArrayBuffer); ok {
	// 		fmt.Println(`ArrayBuffer`)
	// 	} else {
	// 		fmt.Println(reflect.TypeOf(i))
	// 	}
	// }

	return nil
}
