package stream

import (
	"github.com/dop251/goja"
	"gitlab.com/king011/goja_nodejs/util"
)

const (
	// ModuleID .
	ModuleID = `stream`
)

// Require 加載模塊
func Require(vm *goja.Runtime, module *goja.Object) {
	obj := module.Get(`exports`).(*goja.Object)

	wp := &wrapper{
		vm:      vm,
		extends: util.GetExtends(vm),
	}

	obj.Set(`Writable`, wp.Writable())
}
