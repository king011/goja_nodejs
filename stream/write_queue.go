package stream

import (
	"github.com/dop251/goja"
	"gitlab.com/king011_go/deque"
)

type writeElement struct {
	Chunk    goja.Value
	Encoding string
	Callback goja.Callable
}
type writeQueue struct {
	full         *deque.Full
	nodeCapacity int
}

func newWriteQueue(writableHighWaterMark int64) *writeQueue {
	nodeCapacity := int(writableHighWaterMark / 1024)
	if nodeCapacity < 1 {
		nodeCapacity = 1
	}
	return &writeQueue{
		nodeCapacity: nodeCapacity,
		full: deque.NewFull(
			deque.WithFullNodeCapacity(nodeCapacity),
		),
	}
}
func (w *writeQueue) Push(chunk goja.Value, encoding string, callback goja.Callable) bool {
	if !w.full.PushBack(&writeElement{
		Chunk:    chunk,
		Encoding: encoding,
		Callback: callback,
	}) {
		panic(`write queue push false`)
	}
	return w.full.Len() > w.nodeCapacity
}
func (w *writeQueue) IsEmpty() bool {
	return w.full.IsEmpty()
}
func (w *writeQueue) Pop() (chunk goja.Value, encoding string, callback goja.Callable) {
	element := w.full.PopFront().(*writeElement)
	chunk = element.Chunk
	encoding = element.Encoding
	callback = element.Callback
	return
}
