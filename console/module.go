package console

import (
	"fmt"

	"github.com/dop251/goja"
)

const (
	// ModuleID .
	ModuleID = `console`
)

func nativeLog(call goja.FunctionCall) goja.Value {
	count := len(call.Arguments)
	if count == 0 {
		fmt.Println()
		return nil
	}
	arrs := make([]interface{}, count)
	for i := 0; i < count; i++ {
		arg := call.Arguments[i]
		arrs[i] = arg.String()
	}
	fmt.Println(arrs...)
	return nil
}

// Require 加載模塊
func Require(vm *goja.Runtime, module *goja.Object) {
	obj := module.Get(`exports`).(*goja.Object)
	obj.Set(`trace`, nativeLog)
	obj.Set(`debug`, nativeLog)
	obj.Set(`log`, nativeLog)
	obj.Set(`info`, nativeLog)
	obj.Set(`warn`, nativeLog)
	obj.Set(`error`, nativeLog)
}
