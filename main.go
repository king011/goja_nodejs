package main

import (
	"flag"
	"log"
	"path/filepath"

	"github.com/dop251/goja"
	"github.com/dop251/goja_nodejs/require"
	"gitlab.com/king011/goja_nodejs/console"
	"gitlab.com/king011/goja_nodejs/events"
	"gitlab.com/king011/goja_nodejs/helper"
	"gitlab.com/king011/goja_nodejs/stream"

	"gitlab.com/king011/goja_nodejs/util"
)

func main() {
	var (
		help     bool
		filename string
	)
	flag.BoolVar(&help, `help`, false, `display help`)
	flag.Parse()
	if help {
		flag.PrintDefaults()
	} else {
		filename = flag.Arg(0)
		if filename == "" {
			log.Fatalln(`no script specified`)
		}
		var e error
		if filepath.IsAbs(filename) {
			filename = filepath.Clean(filename)
		} else {
			filename, e = filepath.Abs(filename)
			if e != nil {
				log.Fatalln((e))
			}
		}
		source, e := helper.LoadSource(filename)
		if e != nil {
			log.Fatalln(e)
		}
		runScript(filename, source)
	}
}
func runScript(filename, source string) {
	vm := goja.New()

	loop := initModules(vm)
	_, e := vm.RunScript(filename, source)
	if e != nil {
		log.Fatalln(e)
	}

	e = loop.Run()
	if e != nil {
		log.Fatalln(e)
	}
}

func initModules(vm *goja.Runtime) (loop *events.Loop) {
	registry := require.NewRegistry(
		require.WithGlobalFolders(`node_modules`),
		require.WithLoader(helper.SourceLoader),
	)
	registry.Enable(vm)
	// util
	require.RegisterNativeModule(util.ModuleID, util.Require)

	// console
	require.RegisterNativeModule(console.ModuleID, console.Require)
	vm.Set(console.ModuleID, require.Require(vm, console.ModuleID)) // set to global

	// events
	require.RegisterNativeModule(events.ModuleID, events.Require)
	loop = events.GetLoop(vm)
	events.EnableTimer(vm, loop)
	events.EnablePromise(vm, loop)

	// stream
	require.RegisterNativeModule(stream.ModuleID, stream.Require)

	return
}
