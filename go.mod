module gitlab.com/king011/goja_nodejs

go 1.15

require (
	github.com/dlclark/regexp2 v1.4.0 // indirect
	github.com/dop251/goja v0.0.0-20201212162034-be0895b77e07
	github.com/dop251/goja_nodejs v0.0.0-20201201133918-0226646606a0
	github.com/go-sourcemap/sourcemap v2.1.3+incompatible // indirect
	gitlab.com/king011_go/deque v0.0.0-20201213104204-ab65bc255f82
	golang.org/x/text v0.3.4 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
